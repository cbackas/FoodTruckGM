Oklahoma State University - Spring 2019
MSIS 4363: Advanced Application Development

Authors: Zac Gibson and Tyler Marshall

Pretty simple Food Truck tracking app written for Android over about half a semester. Mostly a proof of concept and introduction to new platforms (Android).